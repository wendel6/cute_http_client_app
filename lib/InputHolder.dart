class InputHolder {
  final String firstTitle;
  final String secondTitle;
  final String firstValue;
  final String secondValue;

  InputHolder(
      {this.firstTitle, this.firstValue, this.secondTitle, this.secondValue});

  factory InputHolder.fromJson(Map<String, dynamic> json) {
    return InputHolder(
      firstTitle: json['first_title'],
      secondTitle: json['second_title'],
      firstValue: json['first_value'],
      secondValue: json['second_value'],
    );
  }
}
