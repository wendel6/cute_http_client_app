import 'package:flutter/material.dart';

class JsonDataDisplayRow extends StatelessWidget {
  const JsonDataDisplayRow({
    Key key,
    @required String firstTitle,
    @required TextEditingController controller,
    @required String firstValue,
  })  : _firstTitle = firstTitle,
        _controller = controller,
        _firstValue = firstValue,
        super(key: key);

  final String _firstTitle;
  final TextEditingController _controller;
  final String _firstValue;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              height: 75,
              color: Colors.red,
              child: Center(
                child: Text(
                  _firstTitle,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              height: 75,
              width: 150,
              color: Colors.blueGrey,
              child: Center(
                child: TextField(
                  controller: _controller,
                  textAlign: TextAlign.center,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: _firstValue,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}