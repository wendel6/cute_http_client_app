import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'InputHolder.dart';
import 'ResetAllValues.dart';
import 'JsonDataRow.dart';
import 'MainCenterText.dart';

void main() {
  runApp(MyApp());
}

InputHolder inputHolder = InputHolder();

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _firstTitle = "First Label: blank";
  String _secondTitle = "Second Label: blank";
  String _firstValue = "First Value";
  String _secondValue = "Second Value";

  final TextEditingController _controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MainCenterText(),
            JsonDataDisplayRow(
                firstTitle: _firstTitle,
                controller: _controller,
                firstValue: _firstValue),
            JsonDataDisplayRow(
                firstTitle: _secondTitle,
                controller: _controller,
                firstValue: _secondValue),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 70.0, horizontal: 10),
                  child: Container(
                    height: 75,
                    width: 150,
                    color: Colors.red,
                    child: FlatButton(
                      child: Text(
                        "Fetch Json Data",
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: fetchJsonData,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 70.0, horizontal: 10),
                  child: Container(
                    height: 75,
                    width: 150,
                    color: Colors.blue,
                    child: FlatButton(
                      child: Text(
                        "Clean All Fields",
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: fieldsInitialState,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void fetchJsonData() async {
    InputHolder httpRequest = await httpServerRequest();
    setState(() {
      _firstTitle = httpRequest.firstTitle;
      _secondTitle = httpRequest.secondTitle;
      _firstValue = httpRequest.firstValue;
      _secondValue = httpRequest.secondValue;
    });
  }

  void fieldsInitialState() {
    setState(() {
      _firstTitle = "First Label: blank";
      _secondTitle = "Second Label: blank";
      _firstValue = "First Value";
      _secondValue = "Second Value";
    });
  }
}







