import 'package:flutter/material.dart';

class MainCenterText extends StatelessWidget {
  const MainCenterText({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        "HTTP GET - JSON fetch",
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 30,
        ),
      ),
    );
  }
}