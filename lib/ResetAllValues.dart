import 'InputHolder.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;

void resetAllValues(String a, String b, String c, String d) {}

Future<InputHolder> httpServerRequest() async {
  var url = 'http://3.236.200.0/';
  var response = await http.get(url);

  if (response.statusCode == 200) {
    var inputHolder = InputHolder.fromJson(convert.json.decode(response.body));
    return inputHolder;
  } else {
    print('Request failed with status: ${response.statusCode}.');
  }
}